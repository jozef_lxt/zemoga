//
//  Constants.swift
//  ZemogaTest
//
//  Created by Carlos Mario Muñoz Pérez on 3/11/18.
//  Copyright © 2018 Carlos Mario Muñoz Pérez. All rights reserved.
//

import Foundation


public enum Constants {
    
    enum CommonCells {
        static let postTableViewCell = "PostTableViewCell"
        static let commentTableViewCell = "CommentTableViewCell"
    }
    
    enum PostStatus: Int,  Decodable, Encodable {
        case read = 1, favorite, new
    }
}
