//
//  Environment.swift
//  ZemogaTest
//
//  Created by Carlos Mario Muñoz Pérez on 3/11/18.
//  Copyright © 2018 Carlos Mario Muñoz Pérez. All rights reserved.
//

import Foundation


struct Environment {
    static var baseURL: URL {
        return URL(string: "https://jsonplaceholder.typicode.com/")!
    }
}
