//
//  String+Localization.swift
//  ZemogaTest
//
//  Created by Carlos Mario Muñoz Pérez on 3/11/18.
//  Copyright © 2018 Carlos Mario Muñoz Pérez. All rights reserved.
//

import Foundation


extension String {
    
    func localized() -> String {
        return localized(usingFile: LocalizableStringsFiles.base)
    }
    public func localized(usingFile fileName: String) -> String {
        return NSLocalizedString(self, tableName: fileName, bundle: Bundle.main, value: "", comment: "")
    }
    
}
