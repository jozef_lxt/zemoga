//
//  ApiResponseProtocol.swift
//  ZemogaTest
//
//  Created by Carlos Mario Muñoz Pérez on 3/11/18.
//  Copyright © 2018 Carlos Mario Muñoz Pérez. All rights reserved.
//

import Foundation
import PKHUD

public protocol ApiResponseProtocol : class {
    func apiErrorByCode(error: String?, code: Int?)
}

extension ApiResponseProtocol {
    
    func apiErrorByCode(error: String?, code: Int?) {
        HUD.hide()
        if code == 504 || code == nil {
            HUD.flash(.labeledError(title: "ERROR_NETWORK".localized(), subtitle: nil), delay: 1.0)
        }
        else {
            HUD.flash(.labeledError(title: error, subtitle: nil), delay: 1.0)
        }
    }
}
