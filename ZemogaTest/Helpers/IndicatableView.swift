//
//  IndicatableView.swift
//  ZemogaTest
//
//  Created by Carlos Mario Muñoz Pérez on 3/11/18.
//  Copyright © 2018 Carlos Mario Muñoz Pérez. All rights reserved.
//

import Foundation
import PKHUD

public protocol IndicatableView: class {
    
    func showActivityIndicator()
    
    func showActivityIndicator(title: String?, subtitle: String?)
    
    func hideActivityIndicator()
    
    func showError()
    
    func showSuccess()
    
    func showSuccess(title: String?, subtitle: String?)
    
    func showError(title: String?, subtitle: String?)
    
}
extension IndicatableView where Self: UIViewController {
    
    func showActivityIndicator() {
        showActivityIndicator(title: nil, subtitle: nil)
    }
    
    func showActivityIndicator(title: String?, subtitle: String?) {
        if title != nil || subtitle != nil {
            HUD.flash(.labeledProgress(title: title, subtitle: subtitle), delay: 2.0)
        } else {
            HUD.flash(.progress, delay: 2.0)
        }
    }
    
    func hideActivityIndicator() {
        HUD.hide()
    }
    
    func showError() {
        showError(title: nil, subtitle: nil)
    }
    
    func showSuccess() {
        showSuccess(title: nil, subtitle: nil)
    }
    
    func showSuccess(title: String?, subtitle: String?) {
        if title != nil || subtitle != nil {
            HUD.flash(.labeledSuccess(title: title, subtitle: subtitle), delay: 2.0)
        } else {
            HUD.flash(.success, delay: 2.0)
        }
    }
    
    func showError(title: String?, subtitle: String?) {
        if title != nil || subtitle != nil {
            HUD.flash(.labeledError(title: title, subtitle: subtitle), delay: 2.0)
        } else {
            HUD.flash(.error, delay: 2.0)
        }
    }
}
