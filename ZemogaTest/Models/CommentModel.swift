//
//  ApiResponse+Comment.swift
//  ZemogaTest
//
//  Created by Carlos Mario Muñoz Pérez on 5/11/18.
//  Copyright © 2018 Carlos Mario Muñoz Pérez. All rights reserved.
//

import Foundation


class CommentModel : Decodable, Encodable {
    var id: Int?
    var name: String?
    var email: String?
    var body: String?
}
