//
//  CoreDataContext.swift
//  ZemogaTest
//
//  Created by Carlos Mario Muñoz Pérez on 6/11/18.
//  Copyright © 2018 Carlos Mario Muñoz Pérez. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class CoreDataContext {
    let appDelegate: AppDelegate!
    let context: NSManagedObjectContext!
    init() {
        appDelegate = UIApplication.shared.delegate as? AppDelegate
        context = appDelegate.persistentContainer.viewContext
        

    }
    
    func readPosts() -> [PostModel] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>.init(entityName: "Post")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]
        do {
            let result = try context.fetch(fetchRequest)
            return result.map({ (object) -> PostModel in
                let post = PostModel()
                post.body = (object as! NSManagedObject).value(forKey: "body") as? String
                post.id = (object as! NSManagedObject).value(forKey: "id") as? Int
                post.title = (object as! NSManagedObject).value(forKey: "title") as? String
                post.userId = (object as! NSManagedObject).value(forKey: "userId") as? Int
                post.status = Constants.PostStatus(rawValue: (object as! NSManagedObject).value(forKey: "status") as? Int ?? 0)
                return post
            })
        }
        catch {
            return []
        }
    }
    
    func addPosts(posts: [PostModel]) {
        let postEntity = NSEntityDescription.entity(forEntityName: "Post", in: context)!
        for post in posts {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>.init(entityName: "Post")
            fetchRequest.predicate = NSPredicate(format: "id = %d", post.id ?? 0)
            var modelPost: NSManagedObject?
            do {
                let savedPosts = try context.fetch(fetchRequest)
                if savedPosts.count > 0 {
                    modelPost = savedPosts[0] as? NSManagedObject
                }
            }
            catch {
            }
            if modelPost == nil {
                modelPost = NSManagedObject(entity: postEntity, insertInto: context)
                modelPost!.setValue(post.id, forKey: "id")
            }
            modelPost!.setValue(post.body, forKey: "body")
            modelPost!.setValue(post.userId, forKey: "userId")
            modelPost!.setValue(post.title, forKey: "title")
            do {
                try context.save()
            }
            catch {
                
            }
        }
    }
    
    
    
    func updatePost(id: Int, status: Constants.PostStatus) {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>.init(entityName: "Post")
        fetchRequest.predicate = NSPredicate(format: "id = %d", id)
        do {
            let savedPosts = try context.fetch(fetchRequest)
            if savedPosts.count > 0 {
                let modelPost = savedPosts[0] as? NSManagedObject
                modelPost?.setValue(status.rawValue, forKey: "status")
                try context.save()
            }
        }
        catch {
        }
    }
    
    func deletePost(id: Int) {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>.init(entityName: "Post")
        fetchRequest.predicate = NSPredicate(format: "id = %d", id)
        do {
            let savedPosts = try context.fetch(fetchRequest)
            if savedPosts.count > 0 {
                let modelPost = savedPosts[0] as! NSManagedObject
                context.delete(modelPost)
                try context.save()
            }
        }
        catch {
        }
    }
    
    func deleteAllPosts() {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>.init(entityName: "Post")
        let deleteRequst = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        do {
            try context.execute(deleteRequst)
        }
        catch {
        }
    }
}
