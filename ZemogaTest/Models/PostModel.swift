//
//  ApiResponse+Post.swift
//  ZemogaTest
//
//  Created by Carlos Mario Muñoz Pérez on 3/11/18.
//  Copyright © 2018 Carlos Mario Muñoz Pérez. All rights reserved.
//

import Foundation

class PostModel : Decodable, Encodable {
    var userId: Int?
    var id: Int?
    var title: String?
    var body: String?
    var status: Constants.PostStatus?
}


