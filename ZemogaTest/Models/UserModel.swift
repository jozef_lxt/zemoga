//
//  UserModel.swift
//  ZemogaTest
//
//  Created by Carlos Mario Muñoz Pérez on 6/11/18.
//  Copyright © 2018 Carlos Mario Muñoz Pérez. All rights reserved.
//

import Foundation

class UserModel : Decodable, Encodable {
    var id: Int?
    var name: String?
    var email: String?
    var phone: String?
    var website: String?
}
