//
//  ListListContract.swift
//  ZemogaTest
//
//  Created by Carlos Mario Muñoz on 03/11/2018.
//  Copyright © 2018 Zemoga. All rights reserved.
//

import Foundation
import UIKit

protocol ListView: IndicatableView {
     var presenter: ListPresentation! { get set }
    func updatePosts(posts: [PostModel])
}


protocol ListPresentation: class {
    var view: ListView? { get set }
    var interactor: ListInteraction! { get set }
    var router: ListWireframe! { get set }
    func getPosts()
    func updatePosts()
    func deletePost(id: Int)
    func deleteAllPosts()
    
}

protocol ListInteraction: class {
    var output: ListInteractorOutput? { get set }
    func updatePosts()
    func getPosts() -> [PostModel]
    func deletePost(id: Int)
    func deleteAllPosts()
}

protocol ListInteractorOutput : ApiResponseProtocol {
    func postsUpdated()
}

protocol ListWireframe: class {
    var viewController: UIViewController? { get set }
    static func assembleModule() -> UIViewController
}
