//
//  ListListInteractor.swift
//  ZemogaTest
//
//  Created by Carlos Mario Muñoz on 03/11/2018.
//  Copyright © 2018 Zemoga. All rights reserved.
//

import Foundation

class ListInteractor: ListInteraction{
    
    weak var output: ListInteractorOutput?
    func getPosts() -> [PostModel] {
        return CoreDataContext().readPosts()
    }
    
    func updatePosts() {
        let request = Endpoint.Post()
        ApiClient.sharedInstance.doRequest(req: request) { (apiResult: APIResult<[PostModel]>, code: Int?) in
            switch apiResult{
            case .success(let statusCode, let result):
                switch statusCode{
                case 200:
                    CoreDataContext().addPosts(posts: result)
                    self.output?.postsUpdated()
                default:
                    self.output?.apiErrorByCode(error: "SERVICE_ERROR".localized(), code: statusCode)
                }
            default:
                self.output?.apiErrorByCode(error: "SERVICE_ERROR".localized(), code: code)
                
            }
        }
    }
    
    
    func deletePost(id: Int) {
        CoreDataContext().deletePost(id: id)
    }
    func deleteAllPosts() {
        CoreDataContext().deleteAllPosts()
    }
}
