//
//  ListListPresenter.swift
//  ZemogaTest
//
//  Created by Carlos Mario Muñoz on 03/11/2018.
//  Copyright © 2018 Zemoga. All rights reserved.
//

import Foundation

class ListPresenter : ListPresentation {
    //update database from Api
    func updatePosts() {
        view?.showActivityIndicator()
        interactor.updatePosts()
    }
    
    //Get post from Database
    func getPosts() {
        view?.updatePosts(posts: interactor.getPosts())
    }
    
    
    func deletePost(id: Int) {
        interactor.deletePost(id: id)
    }
    
    func deleteAllPosts() {
        interactor.deleteAllPosts()
    }
    
    weak var view: ListView?
    var interactor: ListInteraction!
    var router: ListWireframe!
    
    
}

extension ListPresenter: ListInteractorOutput{
    //Posts updated, read again from database
    func postsUpdated() {
        view?.hideActivityIndicator()
        getPosts()
    }
}
