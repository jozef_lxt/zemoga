//
//  . ListListRouter.swift
//  ZemogaTest
//
//  Created by Carlos Mario Muñoz on 03/11/2018.
//  Copyright © 2018 Zemoga. All rights reserved.
//

import Foundation
import UIKit

class ListRouter: ListWireframe {
    weak var viewController: UIViewController?
    
    static func assembleModule() -> UIViewController {
        let navigationView = UIStoryboard(name: "List", bundle: nil).instantiateInitialViewController() as! UINavigationController
        let view = navigationView.topViewController as! ListViewController
        let presenter = ListPresenter()
        let interactor = ListInteractor()
        let router = ListRouter()
        
        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.output = presenter
        
        router.viewController = view
        
        return navigationView
    }
    
}
