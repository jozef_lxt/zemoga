//
//  ListListViewController.swift
//  ZemogaTest
//
//  Created by Carlos Mario Muñoz on 03/11/2018.
//  Copyright © 2018 Zemoga. All rights reserved.
//

import Foundation
import UIKit

class ListViewController: UIViewController, ListView {
    var posts: [PostModel] = []
    
    
    @IBOutlet weak var favoriteSegmentedControl: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    var presenter: ListPresentation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.getPosts()
    }
    @IBAction func reloadPosts(_ sender: Any) {
        presenter.updatePosts()
    }
    
    @IBAction func removeAll(_ sender: Any) {
        self.posts = []
        presenter.deleteAllPosts()
        tableView.reloadData()
    }
    @IBAction func seeFavorites(_ sender: Any) {
        tableView.reloadData()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.posts.count > 0 {
            tableView.reloadData()
        }
    }
    func updatePosts(posts: [PostModel]) {
        self.posts = posts
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
}

extension ListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if favoriteSegmentedControl.selectedSegmentIndex == 0 {
            return self.posts.count
        } else {
            return self.posts.filter({ $0.status == .favorite}).count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CommonCells.postTableViewCell) as! PostTableViewCell
        if favoriteSegmentedControl.selectedSegmentIndex == 0 {
        cell.configureCell(item: posts[indexPath.row])
        } else {
             cell.configureCell(item:
            (self.posts.filter({ $0.status == .favorite}))[indexPath.row])
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var post = posts[indexPath.row]
        if favoriteSegmentedControl.selectedSegmentIndex != 0 {
            post = (self.posts.filter({ $0.status == .favorite}))[indexPath.row]
        }
        if post.status == .none {
            post.status = .read
        }
        let detailController = PostDetailRouter.assembleModule(post: post)
        self.navigationController?.pushViewController(detailController, animated: true)
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, index) in
            if self.favoriteSegmentedControl.selectedSegmentIndex != 0 {
                let post = (self.posts.filter({ $0.status == .favorite}))[indexPath.row]
                self.presenter.deletePost(id: post.id ?? 0)
                if let index = self.posts.index(where: {$0.id == post.id}) {
                    self.posts.remove(at: index)
                }
            } else {
                self.presenter.deletePost(id: self.posts[indexPath.row].id ?? 0)
                self.posts.remove(at: indexPath.row)
            }
            self.tableView.reloadData()
        }
        delete.backgroundColor = .red
        return [delete]
    }
    
}
