//
//  PostDetailPostDetailContract.swift
//  ZemogaTest
//
//  Created by Carlos Mario Muñoz on 05/11/2018.
//  Copyright © 2018 Zemoga. All rights reserved.
//

import Foundation
import UIKit

protocol PostDetailView: IndicatableView {
    var post: PostModel! { get set }
    var presenter: PostDetailPresentation! { get set }
    func updateComments(comments: [CommentModel])
    func updateUserData(user: UserModel)
}


protocol PostDetailPresentation: class {
    var view: PostDetailView? { get set }
    var interactor: PostDetailInteraction! { get set }
    var router: PostDetailWireframe! { get set }
    func getUser(userId: Int)
    func getComments(postId: Int)
    func changePostStatus(id: Int, status: Constants.PostStatus)
}

protocol PostDetailInteraction: class {
    var output: PostDetailInteractorOutput? { get set }
    func getUser(userId: Int)
    func getComments(postId: Int)
    func changePostStatus(id: Int, status: Constants.PostStatus)
}

protocol PostDetailInteractorOutput : ApiResponseProtocol {
    func updateComments(comments: [CommentModel])
    func updateUserData(user: UserModel)
}

protocol PostDetailWireframe: class {
    var viewController: UIViewController? { get set }
    static func assembleModule(post: PostModel) -> UIViewController
}
