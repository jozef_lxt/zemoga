//
//  PostDetailPostDetailInteractor.swift
//  ZemogaTest
//
//  Created by Carlos Mario Muñoz on 05/11/2018.
//  Copyright © 2018 Zemoga. All rights reserved.
//

import Foundation

class PostDetailInteractor: PostDetailInteraction{
    
    weak var output: PostDetailInteractorOutput?
    
    func getUser(userId: Int) {
        let request = Endpoint.User(userId: userId)
        ApiClient.sharedInstance.doRequest(req: request) { (apiResult: APIResult<UserModel>, code: Int?) in
            switch apiResult{
            case .success(let statusCode, let result):
                switch statusCode{
                case 200:
                    self.output?.updateUserData(user: result)
                default:
                    self.output?.apiErrorByCode(error: "SERVICE_ERROR".localized(), code: statusCode)
                }
            default:
                self.output?.apiErrorByCode(error: "SERVICE_ERROR".localized(), code: code)
                
            }
        }
    }
    
    func getComments(postId: Int) {
        let request = Endpoint.Comment(postId: postId)
        ApiClient.sharedInstance.doRequest(req: request) { (apiResult: APIResult<[CommentModel]>, code: Int?) in
            switch apiResult{
            case .success(let statusCode, let result):
                switch statusCode{
                case 200:
                    self.output?.updateComments(comments: result)
                default:
                    self.output?.apiErrorByCode(error: "SERVICE_ERROR".localized(), code: statusCode)
                }
            default:
                self.output?.apiErrorByCode(error: "SERVICE_ERROR".localized(), code: code)
                
            }
        }
    }
    
    
    func changePostStatus(id: Int, status: Constants.PostStatus) {
        CoreDataContext().updatePost(id: id, status: status)
    }
    
}
