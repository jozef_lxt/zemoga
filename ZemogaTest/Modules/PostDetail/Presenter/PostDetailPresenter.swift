//
//  PostDetailPostDetailPresenter.swift
//  ZemogaTest
//
//  Created by Carlos Mario Muñoz on 05/11/2018.
//  Copyright © 2018 Zemoga. All rights reserved.
//

import Foundation

class PostDetailPresenter : PostDetailPresentation {
    
    weak var view: PostDetailView?
    var interactor: PostDetailInteraction!
    var router: PostDetailWireframe!
    
    func getUser(userId: Int) {
        view?.showActivityIndicator()
        interactor.getUser(userId: userId)
    }
    
    func getComments(postId: Int) {
        interactor.getComments(postId: postId)
    }
    
    func changePostStatus(id: Int, status: Constants.PostStatus) {
        interactor.changePostStatus(id: id, status: status)
    }
    
}

extension PostDetailPresenter: PostDetailInteractorOutput{
    func updateComments(comments: [CommentModel]) {
        view?.updateComments(comments: comments)
    }
    
    func updateUserData(user: UserModel) {
        view?.hideActivityIndicator()
        view?.updateUserData(user: user)
    }
    
    
    
}
