//
//  . PostDetailPostDetailRouter.swift
//  ZemogaTest
//
//  Created by Carlos Mario Muñoz on 05/11/2018.
//  Copyright © 2018 Zemoga. All rights reserved.
//

import Foundation
import UIKit

class PostDetailRouter: PostDetailWireframe {
    weak var viewController: UIViewController?
    
    static func assembleModule(post: PostModel) -> UIViewController {
        let view = UIStoryboard(name: "PostDetail", bundle: nil).instantiateViewController(withIdentifier: "PostDetailViewController") as! PostDetailViewController
        let presenter = PostDetailPresenter()
        let interactor = PostDetailInteractor()
        let router = PostDetailRouter()
        view.post = post
        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.output = presenter
        
        router.viewController = view
        
        return view
    }
    
}
