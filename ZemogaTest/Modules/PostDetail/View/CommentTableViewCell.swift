//
//  CommentTableViewCell.swift
//  ZemogaTest
//
//  Created by Carlos Mario Muñoz Pérez on 5/11/18.
//  Copyright © 2018 Carlos Mario Muñoz Pérez. All rights reserved.
//

import Foundation
import UIKit

class CommentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func configureCell(item: CommentModel) {
        bodyLabel.text = item.body
        emailLabel.text = item.email
    }
}
