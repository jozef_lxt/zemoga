//
//  PostDetailPostDetailViewController.swift
//  ZemogaTest
//
//  Created by Carlos Mario Muñoz on 05/11/2018.
//  Copyright © 2018 Zemoga. All rights reserved.
//

import Foundation
import UIKit

class PostDetailViewController: UIViewController, PostDetailView {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var websiteLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var favoriteBarButton: UIBarButtonItem!
    @IBOutlet weak var descriptionLabel: UILabel!
    var comments: [CommentModel] = []
    var isFavorite: Bool! {
        didSet {
            post.status = isFavorite ? .favorite : .read
            favoriteBarButton.image = UIImage(named: (isFavorite ?  "whiteCompleteStar" : "whiteStar"))
            presenter.changePostStatus(id: post.id ?? 0, status: post.status ?? .new)
        }
    }
    var post: PostModel!
    var presenter: PostDetailPresentation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isFavorite = post.status == .favorite
        descriptionLabel.text = post.body
        presenter.getComments(postId: post.id ?? 0)
        presenter.getUser(userId: post.userId ?? 0)
    }
    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func markAsFavorite(_ sender: Any) {
        isFavorite = !isFavorite
    }
    
    func updateComments(comments: [CommentModel]) {
        self.comments = comments
        tableView.dataSource = self
        tableView.delegate = self
        tableView.reloadData()
    }
    
    func updateUserData(user: UserModel) {
        nameLabel.text = user.name
        emailLabel.text = user.email
        phoneLabel.text = user.phone
        websiteLabel.text = user.website
    }
}

extension PostDetailViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CommonCells.commentTableViewCell) as! CommentTableViewCell
        cell.configureCell(item: comments[indexPath.row])
        return cell
    }
    
    
}
