//
//  ApiClient.swift
//  ZemogaTest
//
//  Created by Carlos Mario Muñoz Pérez on 3/11/18.
//  Copyright © 2018 Carlos Mario Muñoz Pérez. All rights reserved.
//

import Foundation
import Alamofire
import CodableAlamofire

class ApiClient : SessionManager {
    
    static let sharedInstance = ApiClient()
    
    init() {
        
        let configuration = URLSessionConfiguration.ephemeral
        configuration.urlCache = URLCache.shared
        configuration.requestCachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        configuration.timeoutIntervalForRequest = 20
        configuration.timeoutIntervalForResource = 120
        super.init(configuration: configuration)
        
        
    }
    
    func doRequest<T: Decodable>(req: EndpointProtocol, dateFormat: String = "yyyy/MM/dd HH:mm:ss", completionHandler: @escaping ((APIResult<T>, Int?) -> Void)){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(dateFormatter)
        _ = self
            .request(req)
            .responseDecodableObject(keyPath: nil, decoder: decoder) { (response: DataResponse<T>) in
                var output: APIResult<T>
                let statusCode = response.response?.statusCode
                switch response.result {
                case .success:
                    output = APIResult.success(statusCode!, response.result.value!)
                case .failure(let error):
                    output = APIResult.failure(error)
                }
                completionHandler(output, statusCode)
                
        }
    }
}

