//
//  Endpoint+Comment.swift
//  ZemogaTest
//
//  Created by Carlos Mario Muñoz Pérez on 5/11/18.
//  Copyright © 2018 Carlos Mario Muñoz Pérez. All rights reserved.
//

import Foundation
import Alamofire

extension Endpoint {
    public struct Comment {
        var postId: Int?
    }
}

extension Endpoint.Comment: EndpointProtocol {
    
    public var description: String { return "Endpoint.Comment" }
    public var path: String { return "comments" }
    public var method: HTTPMethod { return .get }
    public var headers: HTTPHeaders? {return nil}
    public var encoding: ParameterEncoding { return Alamofire.JSONEncoding.default }
    public var params: Parameters? {return nil}
    public var queryItems: [URLQueryItem]? {
        return [URLQueryItem(name: "postId", value: String(postId ?? 0))]
    }
}
