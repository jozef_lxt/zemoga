//
//  Endpoint+Post.swift
//  ZemogaTest
//
//  Created by Carlos Mario Muñoz Pérez on 3/11/18.
//  Copyright © 2018 Carlos Mario Muñoz Pérez. All rights reserved.
//

import Foundation
import Alamofire

extension Endpoint {
    public struct Post {
    }
}

extension Endpoint.Post: EndpointProtocol {
    
    public var description: String { return "Endpoint.Post" }
    public var path: String { return "posts" }
    public var method: HTTPMethod { return .get }
    public var headers: HTTPHeaders? {return nil}
    public var encoding: ParameterEncoding { return Alamofire.JSONEncoding.default }
    public var params: Parameters? {return nil}
    public var queryItems: [URLQueryItem]? {
        return nil
    }
}
