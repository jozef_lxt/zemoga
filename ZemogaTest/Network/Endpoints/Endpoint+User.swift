//
//  Endpoint+User.swift
//  ZemogaTest
//
//  Created by Carlos Mario Muñoz Pérez on 5/11/18.
//  Copyright © 2018 Carlos Mario Muñoz Pérez. All rights reserved.
//

import Foundation
import Alamofire

extension Endpoint {
    public struct User {
        var userId: Int?
    }
}

extension Endpoint.User: EndpointProtocol {
    
    public var description: String { return "Endpoint.User" }
    public var path: String { return "users/\(userId ?? 0)" }
    public var method: HTTPMethod { return .get }
    public var headers: HTTPHeaders? {return nil}
    public var encoding: ParameterEncoding { return Alamofire.JSONEncoding.default }
    public var params: Parameters? {return nil}
    public var queryItems: [URLQueryItem]? {
        return nil
    }
}
