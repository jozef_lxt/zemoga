//
//  Endpoint.swift
//  ZemogaTest
//
//  Created by Carlos Mario Muñoz Pérez on 3/11/18.
//  Copyright © 2018 Carlos Mario Muñoz Pérez. All rights reserved.
//

import Foundation
import Alamofire

public protocol EndpointProtocol: URLRequestConvertible {
    var baseURL: URL { get }
    var path: String { get }
    var description: String { get }
    var method: HTTPMethod { get }
    var headers: HTTPHeaders? { get }
    var params: Parameters? { get }
    var encoding: ParameterEncoding { get }
    var queryItems: [URLQueryItem]?{get}
}

extension EndpointProtocol {
    
    public var baseURL: URL {
        return Environment.baseURL
    }
    
    func encode(_ urlRequest: URLRequestConvertible) throws -> URLRequest{
        return try encoding.encode(urlRequest, with: params)
    }
    
    public func asURLRequest() throws -> URLRequest {
        var urlComponent = URLComponents(string: baseURL.appendingPathComponent(path).absoluteString)!
        urlComponent.queryItems = queryItems
        var urlRequest = URLRequest(url: urlComponent.url!)
        urlRequest.httpMethod = method.rawValue
        urlRequest.allHTTPHeaderFields = headers
        headers?.forEach { urlRequest.setValue($0.value, forHTTPHeaderField: $0.key) }
        urlRequest = try encode(urlRequest)
        return urlRequest
    }
    
}

public struct Endpoint {}
