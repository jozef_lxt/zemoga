//
//  ApiResponse.swift
//  ZemogaTest
//
//  Created by Carlos Mario Muñoz Pérez on 3/11/18.
//  Copyright © 2018 Carlos Mario Muñoz Pérez. All rights reserved.
//

import Foundation

public enum APIResult<T> {
    case success(Int, T)
    case failure(Error)
}
