//
//  IndicatableViewTest.swift
//  ZemogaTestTests
//
//  Created by Carlos Mario Muñoz Pérez on 6/11/18.
//  Copyright © 2018 Carlos Mario Muñoz Pérez. All rights reserved.
//

import Foundation
@testable import ZemogaTest

class IndicatableViewTest: IndicatableView {
    func showActivityIndicator() {
        
    }
    
    func showActivityIndicator(title: String?, subtitle: String?) {
        
    }
    
    func hideActivityIndicator() {
        
    }
    
    func showError() {
        
    }
    
    func showSuccess() {
        
    }
    
    func showSuccess(title: String?, subtitle: String?) {
        
    }
    
    func showError(title: String?, subtitle: String?) {
        
    }
    
    
}
