//
//  ListListInteractorTests.swift
//  ZemogaTest
//
//  Created by Carlos Mario Muñoz on 03/11/2018.
//  Copyright © 2018 Zemoga. All rights reserved.
//

import Foundation
import XCTest
@testable import ZemogaTest

class ListInteractorTest: XCTestCase{
    
    var listInteractor: ListInteractor!
    override func setUp() {
        super.setUp()
        listInteractor = ListInteractor()
    }
    
    override func tearDown() {
        super.tearDown()
        listInteractor = nil
    }
}

class MockListInteractorOutput: InteractorOutputTest, ListInteractorOutput {
    func postsUpdated() {
    }
    
    
}
