//
//  ListListPresenterTests.swift
//  ZemogaTest
//
//  Created by Carlos Mario Muñoz on 03/11/2018.
//  Copyright © 2018 Zemoga. All rights reserved.
//


import Foundation
import XCTest
@testable import ZemogaTest

class ListPresenterTest: XCTestCase {
    var listPresenter: ListPresenter!
    var mockListView: MockListView!
    var mockListInteractor: MockListInteractor!
    override func setUp() {
        super.setUp()
        listPresenter = ListPresenter()
        mockListView = MockListView()
        mockListInteractor = MockListInteractor()
        listPresenter.view = mockListView
        listPresenter.interactor = mockListInteractor
    }
    
    override func tearDown() {
        super.tearDown()
        listPresenter = nil
    }
    
}
class MockListView : IndicatableViewTest, ListView {
    func updatePosts(posts: [PostModel]) {
        
    }
    
    var presenter: ListPresentation!
}

class MockListInteractor : ListInteraction {
    func updatePosts() {
        
    }
    
    func getPosts() -> [PostModel] {
        return []
    }
    
    func deletePost(id: Int) {
        
    }
    
    func deleteAllPosts() {
        
    }
    
    weak var output: ListInteractorOutput?
}
