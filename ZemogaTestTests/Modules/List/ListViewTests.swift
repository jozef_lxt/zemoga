//
//  ListListViewTests.swift
//  ZemogaTest
//
//  Created by Carlos Mario Muñoz on 03/11/2018.
//  Copyright © 2018 Zemoga. All rights reserved.
//

import Foundation
import XCTest
@testable import ZemogaTest

class ListViewTest: XCTestCase{
    var mockListPresenter: MockListPresenter!
    var controllerUnderTest: ListViewController!
    override func setUp() {
        super.setUp()
        mockListPresenter = MockListPresenter()
        controllerUnderTest = UIStoryboard(name: "ListStoryboard", bundle: nil).instantiateViewController(withIdentifier: "ListViewController") as? ListViewController
        controllerUnderTest.presenter = mockListPresenter
        controllerUnderTest.loadViewIfNeeded()
    }
    
    override func tearDown() {
        super.tearDown()
        controllerUnderTest = nil
        mockListPresenter = nil
    }
    
}
class MockListPresenter : ListPresentation {
    func getPosts() {
        
    }
    
    func updatePosts() {
        
    }
    
    func deletePost(id: Int) {
        
    }
    
    func deleteAllPosts() {
        
    }
    
    weak var view: ListView?
    var interactor: ListInteraction!
    
    var router: ListWireframe!
    
    func viewDidLoad() {
        
    }
}

