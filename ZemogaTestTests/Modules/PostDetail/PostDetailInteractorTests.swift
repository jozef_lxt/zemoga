//
//  PostDetailPostDetailInteractorTests.swift
//  ZemogaTest
//
//  Created by Carlos Mario Muñoz on 05/11/2018.
//  Copyright © 2018 Zemoga. All rights reserved.
//

import Foundation
import XCTest
@testable import ZemogaTest

class PostDetailInteractorTest: XCTestCase{
    
    var postdetailInteractor: PostDetailInteractor!
    override func setUp() {
        super.setUp()
        postdetailInteractor = PostDetailInteractor()
    }
    
    override func tearDown() {
        super.tearDown()
        postdetailInteractor = nil
    }
}

class MockPostDetailInteractorOutput: InteractorOutputTest, PostDetailInteractorOutput {
    func updateComments(comments: [CommentModel]) {
        
    }
    
    func updateUserData(user: UserModel) {
        
    }
    
    
}
