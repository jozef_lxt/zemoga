//
//  PostDetailPostDetailPresenterTests.swift
//  ZemogaTest
//
//  Created by Carlos Mario Muñoz on 05/11/2018.
//  Copyright © 2018 Zemoga. All rights reserved.
//


import Foundation
import XCTest
@testable import ZemogaTest

class PostDetailPresenterTest: XCTestCase {
    var postdetailPresenter: PostDetailPresenter!
    var mockPostDetailView: MockPostDetailView!
    var mockPostDetailInteractor: MockPostDetailInteractor!
    override func setUp() {
        super.setUp()
        postdetailPresenter = PostDetailPresenter()
        mockPostDetailView = MockPostDetailView()
        mockPostDetailInteractor = MockPostDetailInteractor()
        postdetailPresenter.view = mockPostDetailView
        postdetailPresenter.interactor = mockPostDetailInteractor
    }
    
    override func tearDown() {
        super.tearDown()
        postdetailPresenter = nil
    }
    
}
class MockPostDetailView : IndicatableViewTest, PostDetailView {
    var post: PostModel!
    
    func updateComments(comments: [CommentModel]) {
        
    }
    
    func updateUserData(user: UserModel) {
        
    }
    
    var presenter: PostDetailPresentation!
}

class MockPostDetailInteractor : PostDetailInteraction {
    func getUser(userId: Int) {
        
    }
    
    func getComments(postId: Int) {
        
    }
    
    func changePostStatus(id: Int, status: Constants.PostStatus) {
        
    }
    
    weak var output: PostDetailInteractorOutput?
}
