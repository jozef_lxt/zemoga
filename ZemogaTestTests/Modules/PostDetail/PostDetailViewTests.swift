//
//  PostDetailPostDetailViewTests.swift
//  ZemogaTest
//
//  Created by Carlos Mario Muñoz on 05/11/2018.
//  Copyright © 2018 Zemoga. All rights reserved.
//

import Foundation
import XCTest
@testable import ZemogaTest

class PostDetailViewTest: XCTestCase{
    var mockPostDetailPresenter: MockPostDetailPresenter!
    var controllerUnderTest: PostDetailViewController!
    override func setUp() {
        super.setUp()
        mockPostDetailPresenter = MockPostDetailPresenter()
        controllerUnderTest = UIStoryboard(name: "PostDetailStoryboard", bundle: nil).instantiateViewController(withIdentifier: "PostDetailViewController") as? PostDetailViewController
        controllerUnderTest.presenter = mockPostDetailPresenter
        controllerUnderTest.loadViewIfNeeded()
    }
    
    override func tearDown() {
        super.tearDown()
        controllerUnderTest = nil
        mockPostDetailPresenter = nil
    }
    
}
class MockPostDetailPresenter : PostDetailPresentation {
    func getUser(userId: Int) {
        
    }
    
    func getComments(postId: Int) {
        
    }
    
    func changePostStatus(id: Int, status: Constants.PostStatus) {
        
    }
    
    weak var view: PostDetailView?
    var interactor: PostDetailInteraction!
    
    var router: PostDetailWireframe!
    
    func viewDidLoad() {
        
    }
}

